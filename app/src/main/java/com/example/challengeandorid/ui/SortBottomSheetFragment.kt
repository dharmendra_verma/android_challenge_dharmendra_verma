package com.example.challengeandorid.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.example.challengeandorid.R
import com.example.challengeandorid.enum.SortEnum
import com.example.challengeandorid.viewmodel.MainActivityViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_options.view.*

/**
 * The Bottom Sheet dialog fragment which appears when fab button is clicked
 */

class SortBottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var mainActivityViewModel: MainActivityViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.bottom_sheet_options, container, false)

        activity?.let {
            mainActivityViewModel = ViewModelProviders.of(it).get(MainActivityViewModel::class.java)
        }
        //handle clicks
        v.cheapest.setOnCheckedChangeListener { e, isChecked ->
            if (isChecked) {
                dismiss()
                mainActivityViewModel.sortFlights(SortEnum.CHEAPEST)
            }
        }
        v.earliest.setOnCheckedChangeListener { e, isChecked ->
            if (isChecked) {
                dismiss()
                mainActivityViewModel.sortFlights(SortEnum.EARLIEST)
            }
        }
        v.fastest.setOnCheckedChangeListener { e, isChecked ->
            if (isChecked) {
                dismiss()
                mainActivityViewModel.sortFlights(SortEnum.FASTEST)
            }
        }
        return v
    }
}
