package com.example.challengeandorid.source

import FlightsResponse
import com.example.challengeandorid.source.remote.FlightsRemoteDataSource
import retrofit2.Call

/**
 * Repository Class to fetch data
 */

object FlightsRepository : FlightsDataSource {

    private val mPhotoRemoteDataSource = FlightsRemoteDataSource()

    override fun getFlights(): Call<FlightsResponse> {
        return mPhotoRemoteDataSource.getFlights();
    }
}