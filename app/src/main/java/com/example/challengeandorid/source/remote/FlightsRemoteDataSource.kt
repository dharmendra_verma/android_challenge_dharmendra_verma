package com.example.challengeandorid.source.remote

import FlightsResponse
import com.example.challengeandorid.network.NetworkApi
import com.example.challengeandorid.network.NetworkService
import com.example.challengeandorid.util.Constant
import retrofit2.Call

/**
 * Remote Data Source Class when we can will fetch data from Network
 */

class FlightsRemoteDataSource {
    private val mFlightsApi =
        NetworkService.createRetrofit(NetworkApi::class.java, Constant.BASE_URL)

    fun getFlights(): Call<FlightsResponse> {

        return mFlightsApi.getFlights()
    }
}