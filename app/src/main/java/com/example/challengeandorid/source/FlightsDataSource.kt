package com.example.challengeandorid.source

import FlightsResponse
import retrofit2.Call

/**
 * Interface to fetch data
 */

interface FlightsDataSource {

    fun getFlights(): Call<FlightsResponse>
}