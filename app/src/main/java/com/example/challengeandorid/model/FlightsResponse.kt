import com.google.gson.annotations.SerializedName

/**
 * Json Response after fetching flights
 */

data class FlightsResponse(

    @SerializedName("appendix") val appendix: Appendix,
    @SerializedName("flights") val flights: List<Flights>
)