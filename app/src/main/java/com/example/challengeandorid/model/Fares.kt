import com.google.gson.annotations.SerializedName

/**
 * FareModelClass
 */

data class Fares(

    @SerializedName("providerId") var providerId: Int,
    @SerializedName("fare") var fare: Int
)