import com.google.gson.annotations.SerializedName

/**
 * Airlines, Airports And Providers Map
 */

data class Appendix(

    @SerializedName("airlines") val airlines: Map<String, String>,
    @SerializedName("airports") val airports: Map<String, String>,
    @SerializedName("providers") val providers: Map<Int, String>
)