import com.google.gson.annotations.SerializedName

/**
 * Flights Model Class
 */

data class Flights(

    @SerializedName("originCode") val originCode: String,
    @SerializedName("destinationCode") val destinationCode: String,
    @SerializedName("departureTime") val departureTime: Long,
    @SerializedName("arrivalTime") val arrivalTime: Long,
    @SerializedName("fares") val fares: List<Fares>,
    @SerializedName("airlineCode") val airlineCode: String,
    @SerializedName("class") val `class`: String
)