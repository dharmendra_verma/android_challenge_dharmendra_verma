import com.google.gson.annotations.SerializedName

/**
 * All Airlines Data
 */

data class Airlines(

    @SerializedName("SG") val flight_sG: String,
    @SerializedName("AI") val flight_aI: String,
    @SerializedName("G8") val flight_g8: String,
    @SerializedName("9W") val flight_9W: String,
    @SerializedName("6E") val flight_6E: String
)