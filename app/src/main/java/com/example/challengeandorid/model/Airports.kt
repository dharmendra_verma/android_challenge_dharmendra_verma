import com.google.gson.annotations.SerializedName

/**
 * All Airports Data
 */

data class Airports(

    @SerializedName("DEL") val dEL: String,
    @SerializedName("BOM") val bOM: String
)