package com.example.challengeandorid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengeandorid.adapters.FlightsAdapter
import com.example.challengeandorid.ui.SortBottomSheetFragment
import com.example.challengeandorid.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainActivityViewModel: MainActivityViewModel
    private lateinit var flightsAdapter: FlightsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupFab()
        setUpRecyclerView()
        setUpObserver()
        setupFab()
        setActionBar()
    }

    /**
     * Method to get ViewModel & Observer the Live Data
     */

    private fun setUpObserver() {
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)

        mainActivityViewModel.fetchFlights().observe(this, Observer {
            flightsAdapter.addAll(it)
            flightsRecyclerView.smoothScrollToPosition(0)
        })

        mainActivityViewModel.progressBarVisibility.observe(this, Observer {
            progressBar.visibility = if (it) {
                View.VISIBLE
            } else View.GONE
        })
    }

    /**
     * Method to setup RecyclerView
     */
    private fun setUpRecyclerView() {
        flightsAdapter = FlightsAdapter()
        flightsRecyclerView.layoutManager = LinearLayoutManager(this)
        flightsRecyclerView.adapter = flightsAdapter
    }

    /**
     * Method to setup bottom navigation bar on clinking Fab Button
     */
    private fun setupFab() {
        sortFlight.setOnClickListener() {
            SortBottomSheetFragment().show(supportFragmentManager, "Sort")
        }
    }

    /**
     * Method to setup title toolbar
     */
    private fun setActionBar() {
        actionBar?.setTitle("DEL ➝ MUM")
        supportActionBar?.setTitle("DEL ➝ MUM")
    }
}