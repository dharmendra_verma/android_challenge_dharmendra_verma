package com.example.challengeandorid.util

import Fares
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

/**
 * Utils Class to add all the Flight Utils Functions
 */

object FlightsUtils {

    /**
     * Method to get duration in hour min text format for a given arrival and departure time
     * @param arrivalTime the arrival timestamp
     * @param departureTime the depature timestamp
     */
    fun getDuration(arrivalTime: Long, departureTime: Long): String {
        val startDate = Date(arrivalTime)
        val endDate = Date(departureTime)
        val diff = abs(endDate.time - startDate.time)
        val diffMinutes: Long = diff / (60 * 1000) % 60
        val diffHours: Long = diff / (60 * 60 * 1000) % 24
        return "${diffHours}h ${diffMinutes}m"
    }

    /**
     * Method to get time in  hour:minute format for a given timestamp
     * @param timestamp the provide timestamp
     */
    fun getTimeInHourMin(timestamp: Long): String {
        val formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
        return formatter.format(Date(timestamp))
    }

    /**
     * Method to get best fare from the fares list
     * @param fares the fares list
     */
    fun getBestPrice(fares: List<Fares>): Fares {
        val bestFares = Fares(1, Int.MAX_VALUE)
        fares.forEach { fare ->
            if (fare.fare < bestFares.fare) {
                bestFares.fare = fare.fare
                bestFares.providerId = fare.providerId
            }
        }
        return bestFares
    }

    /**
     * Method to get sort duration in timestamp format
     * @param timestampStart the start timestamp
     * @param timestampEnd the end timestamp
     */
    fun getSortDuration(timestampStart: Long, timestampEnd: Long): Long {
        val startDate = Date(timestampStart)
        val endDate = Date(timestampEnd)
        return abs(endDate.time - startDate.time)
    }
}