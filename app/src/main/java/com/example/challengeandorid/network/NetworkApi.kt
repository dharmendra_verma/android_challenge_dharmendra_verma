package com.example.challengeandorid.network

import FlightsResponse
import retrofit2.Call
import retrofit2.http.GET

/**
 * RetroFit Interface
 */

interface NetworkApi {

    @GET("/v2/5979c6731100001e039edcb3")
    fun getFlights(): Call<FlightsResponse>
}