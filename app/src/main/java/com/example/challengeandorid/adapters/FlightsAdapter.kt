package com.example.challengeandorid.adapters

import Appendix
import Flights
import FlightsResponse
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.challengeandorid.R
import com.example.challengeandorid.util.FlightsUtils
import kotlinx.android.synthetic.main.flights_item.view.*
import kotlinx.android.synthetic.main.providers_item.view.*

/**
 * FlightAdapterClass with FlightViewHolder to Handle the Adapter Data corresponding to the RecyclerView
 */

class FlightsAdapter : RecyclerView.Adapter<FlightsAdapter.FlightsViewHolder>() {

    private lateinit var appendix: Appendix
    private val flightList: ArrayList<Flights> = ArrayList()

    /**
     * add FlightResponse to Handle the Adapter Data
     */

    fun addAll(flightsResponse: FlightsResponse) {
        this.appendix = flightsResponse.appendix
        flightList.clear()
        flightList.addAll(flightsResponse.flights)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightsViewHolder {
        return FlightsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.flights_item,
                parent,
                false
            )
        )
    }

    override fun getItemViewType(position: Int): Int {
        return flightList[position].hashCode()
    }

    override fun getItemCount(): Int {
        return flightList.size
    }


    override fun onBindViewHolder(holder: FlightsViewHolder, position: Int) {
        flightList[position].let { flight ->
            holder.bind(flight, appendix)
        }
    }

    class FlightsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(flights: Flights, appendix: Appendix) {
            itemView.flClass.text = flights.`class`
            itemView.flDuration.text =
                FlightsUtils.getDuration(flights.arrivalTime, flights.departureTime)
            itemView.flinfo.text = itemView.context.getString(
                R.string.source_destination, flights.originCode,
                flights.destinationCode
            )
            itemView.flTime.text = itemView.context.getString(
                R.string.time,
                FlightsUtils.getTimeInHourMin(flights.arrivalTime),
                FlightsUtils.getTimeInHourMin(flights.departureTime)
            )
            itemView.flName.text = appendix.airlines[flights.airlineCode]

            val bestFares = FlightsUtils.getBestPrice(flights.fares)
            itemView.flPrice.text = itemView.context.getString(
                R.string.price, bestFares.fare
            )
            itemView.flProvider.text = appendix.providers[bestFares.providerId]

            itemView.otherProviderView.setOnClickListener {
                if (itemView.otherProvidersLayout.visibility == View.GONE) {
                    itemView.fareOtherProviderMessage.text = "Hide Fare From Other Providers"
                    itemView.otherProvidersLayout.visibility = View.VISIBLE
                } else {
                    itemView.fareOtherProviderMessage.text = "View Fare From Other Providers"
                    itemView.otherProvidersLayout.visibility = View.GONE
                }
            }

            // Do not repopulate the otherProviderDetails
            if (itemView.otherProvidersLayout.childCount > 0) {
                return
            }

            // Do not show the otherProviderView when there's only one provider
            if (flights.fares.size == 1) {
                itemView.otherProviderView.visibility = View.GONE
                return
            }

            val inflater = LayoutInflater.from(itemView.context)
            flights.fares.forEach { (providerId, price) ->
                if (providerId != bestFares.providerId) {
                    val providerView =
                        inflater.inflate(
                            R.layout.providers_item,
                            itemView.otherProvidersLayout,
                            false
                        )
                    providerView.providerName.text = appendix.providers[providerId]
                    providerView.providerFare.text = itemView.context.getString(
                        R.string.price, price
                    )
                    itemView.otherProvidersLayout.addView(providerView)
                }
            }
        }

    }
}