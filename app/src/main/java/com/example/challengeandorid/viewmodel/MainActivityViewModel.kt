package com.example.challengeandorid.viewmodel

import FlightsResponse
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengeandorid.enum.SortEnum
import com.example.challengeandorid.source.FlightsRepository
import com.example.challengeandorid.util.FlightsUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityViewModel : ViewModel() {

    var flightDetails: MutableLiveData<FlightsResponse> = MutableLiveData()
    var progressBarVisibility: MutableLiveData<Boolean> = MutableLiveData(true)

    /**
     * Method to fetch Flights from the Repository
     */
    fun fetchFlights(): LiveData<FlightsResponse> {
        progressBarVisibility.value = true
        FlightsRepository
            .getFlights()
            .enqueue(object : Callback<FlightsResponse> {
                override fun onResponse(
                    call: Call<FlightsResponse>,
                    response: Response<FlightsResponse>
                ) {
                    progressBarVisibility.value = false
                    flightDetails.value = response.body()
                }

                override fun onFailure(call: Call<FlightsResponse>, t: Throwable) {
                    progressBarVisibility.value = false
                }
            })
        return flightDetails
    }

    /**
     * Method to sort the Flights List according to the SortType
     * @param sortType contains the requested sortType
     */

    fun sortFlights(sortType: SortEnum) {
        val flightInfo = (flightDetails.value as FlightsResponse)
        when (sortType) {
            SortEnum.CHEAPEST -> {
                flightDetails.value =
                    FlightsResponse(
                        flightInfo.appendix,
                        flightInfo.flights.sortedBy {
                            FlightsUtils.getBestPrice(it.fares).fare
                        })
            }
            SortEnum.EARLIEST -> {
                flightDetails.value =
                    FlightsResponse(
                        flightInfo.appendix,
                        flightInfo.flights.sortedBy {
                            it.departureTime
                        })
            }
            SortEnum.FASTEST -> {
                flightDetails.value =
                    FlightsResponse(
                        flightInfo.appendix,
                        flightInfo.flights.sortedBy {
                            FlightsUtils.getSortDuration(it.arrivalTime, it.departureTime)
                        })
            }
        }
    }
}