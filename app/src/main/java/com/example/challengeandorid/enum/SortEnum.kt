package com.example.challengeandorid.enum

/**
 * Enum Type SortClass
 */

enum class SortEnum {
    CHEAPEST,
    EARLIEST,
    FASTEST
}